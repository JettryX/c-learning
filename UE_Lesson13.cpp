﻿// UE_Lesson13.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <locale>
#include <string>
#include "Functions.h"
using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");
    
    string inputA;
    string inputB;
    int A;
    int B;

    bool valid = false;

    cout << "---O.O--- | Программа суммирует 2 числа (A и B) и выводит их квадрат суммы | ---O.O---" << endl;
    while (!valid) {
        cout << " Введите число A: ";
        getline(cin, inputA);
        cout << " Введите число B: ";
        getline(cin, inputB);
        try {
            A = stoi(inputA);
            B = stoi(inputB);
            valid = true;
        }
        catch (...) {
            valid = false;
            cout << " Некорректные данные, попробуйте сново. \n";
            continue;
        }

        cout << "Результат: " << summSquare(A, B);
        cout << endl;

        cout << "Продолжить? (Да(1) | любой другой ввод закроет программу): ";
        getline(cin, inputA);
        try {
            if (stoi(inputA) == 1) {
                valid = false;
                continue;
            }
        }
        catch (...) {}
        return 0;
    }

}
