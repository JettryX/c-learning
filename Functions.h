#pragma once
#include <math.h>

int summSquare(int a, int b) {
	return pow(a + b, 2);
}